*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot



*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${Month}  //div[@class='bs-datepicker-container ng-tns-c154-1 ng-trigger ng-trigger-datepickerAnimation']/div/div/bs-days-calendar-view[1]/bs-calendar-layout/div[1]/bs-datepicker-navigation-view/button[2]
${Month-August}  //div[@class='bs-datepicker-container ng-tns-c154-1 ng-trigger ng-trigger-datepickerAnimation']/div/div/bs-month-calendar-view[1]/bs-calendar-layout/div[2]/table/tbody/tr/td/span[text()='August']
${Day}   //div[@class='bs-datepicker-container ng-tns-c154-1 ng-trigger ng-trigger-datepickerAnimation']/div/div/bs-days-calendar-view[1]/bs-calendar-layout/div[2]/table/tbody/tr/td/span[text()='17']
${CreatedDate}  17/08/2021
${Exit}
*** Keywords ***



    
Click btn Filter
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval-loa  10s
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    ${CheckFilter}  Run Keyword And Return Status      Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[4]/div/input  10s
    Run Keyword If  '${CheckFilter}' != 'True'  Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Click Element  //div[@class='card table-filter']/div/div[4]/div/input
    

Choose Created Date
    Click Element  ${Month}
    Click Element  ${Month-August}
    Click Element  ${Day}
    Click Element  ${Day}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT114-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(117,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData}=	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Show}  Get Text  //tbody/tr[1]/td[5]   
                    IF   '${Show}' == '${CreatedDate}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT114-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(117,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT114-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(117,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                    ${Show}  Get Text  //tbody/tr[${n}]/td[5]
                    IF    '${Show}' == '${CreatedDate}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT114-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(117,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT114-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(117,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
         Exit For Loop IF  "${Exit}" == "Exit"  
         END
         Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END



*** Test Cases ***
ApproveLOA - Filter_CreatedDate
    Open Website SMARTFLOW
    Approve - Audit_director
    Click Btn Login 
    Input OTP And Click OTP
    Choose Tab LOA 
    Click btn Filter
    Choose Created Date
  
    
    
  
    
  
    
    
   
    
    

    
    
    