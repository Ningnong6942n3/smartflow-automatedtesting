*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***

${excel}   SmartFlow.xlsx

*** Keywords ***


Click btn Edit
    Click Button  //div[@class='header-content']/button
    Sleep  2
    Input text  id=subject  ทดสอบการสร้างเอกสาร02
    Sleep  2
    Click Button  //div[@class='text-center my-4']/button[2]
    Sleep  2
    
Check Change DocumentDetails    
    ${Message}  Get Text  //div[@class='row p-0 m-0']/div/div/div/p[text()='ทดสอบการสร้างเอกสาร02']
    IF    '${Message}' == 'ทดสอบการสร้างเอกสาร02'
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT15-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(18,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT15-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(18,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END

*** Test Cases ***
CreateDocument-Edit2
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Choose For
    Input Subject 
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Click Btn Next
    Click btn Edit
    Check Change DocumentDetails   
    
  
    
    
    