*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
*** Keywords ***

Click Btn Filter - Completed
    Wait Until Element Is Visible  //tbody/tr/td  30s
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[3]  10s
    Click Element  //div[@class='card table-filter']/div/div[3]
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[5]  10s
    Click Element  //ng-dropdown-panel/div/div/div[5]
    ${count} =	Get Element Count  //tbody/tr
    Log To Console  ${count}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    Log to Console   ${Show}
    FOR    ${i}    IN RANGE    ${count}
     ${n}  Evaluate  ${i}+1
     IF    ${count} == ${NUMBER}
          IF    '${Show}' == 'No matching records found'
            Log to Console   No matching records found
          ELSE IF  '${Show}' == '${Show}'
            ${Show}  Get Text  //tbody/tr[1]/td[9]/button
            Should Contain    Completed    ${Show}  
          END
     ELSE IF    ${count} > ${NUMBER}
       ${Show}  Get Text  //tbody/tr[${n}]/td[9]/button
       Should Contain    Completed    ${Show}  
     END
    END 
    ${Subject}  Get Text  //tbody/tr[1]/td[1]/button/p
    Set Global Variable  ${Subject}
    ${DocumentNo}  Get Text  //tbody/tr[1]/td[7]/div/button[1]
    Set Global Variable  ${DocumentNo}
    Click Button  //tbody/tr[1]/td[10]/button
    Wait Until Element Is Visible  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/span/small  10s
    ${CreatedbyAll}  Get Text  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/span/small
    ${CreatedbyCut}  Fetch From Right  ${CreatedbyAll}  Created by :
    ${Createdby}  Fetch From Left  ${CreatedbyCut}   Created date :
    Set Global Variable  ${Createdby}
    Log To Console  ${Createdby}
    Wait Until Element Is Visible  //div[@class='ng2-pdf-viewer-container']/div[@class='pdfViewer removePageBorders']/div/div[2]/span  10s
    ${countv} =	Get Element Count  //div[@class='ng2-pdf-viewer-container']/div[@class='pdfViewer removePageBorders']/div/div[2]/span
    Log To Console  ${countv}
    Wait Until Element Is Visible  //div[@class='ng2-pdf-viewer-container']/div[@class='pdfViewer removePageBorders']/div/div[2]/span[${countv}]  10s
    ${ValidationCode}  Get Text  //div[@class='ng2-pdf-viewer-container']/div[@class='pdfViewer removePageBorders']/div/div[2]/span[${countv}]
    Log To Console  ${ValidationCode}
    Set Global Variable  ${ValidationCode}
    Wait Until Element Is Visible  //div[@class='d-flex flex-row']/div[@class='right-wrapper d-sm-none d-md-block d-none']/div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[3]  10s
    Click Element  //div[@class='d-flex flex-row']/div[@class='right-wrapper d-sm-none d-md-block d-none']/div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[3]
    Wait Until Element Is Visible  //div[@class='d-flex flex-row']/div[@class='right-wrapper d-sm-none d-md-block d-none']/div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/div/div/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[3]  10s
    ${VerifiedDate}  Get Text  //div[@class='d-flex flex-row']/div[@class='right-wrapper d-sm-none d-md-block d-none']/div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/div/div/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[3]
    Set Global Variable  ${VerifiedDate}
    Log To Console  ${VerifiedDate} 
    ${CreateDate}  Get Text   //div[@class='d-flex flex-row']/div[@class='right-wrapper d-sm-none d-md-block d-none']/div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/div/div/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[4]/p[3]
    Set Global Variable  ${CreateDate}
    Log To Console  ${CreateDate} 

Choose Tab Verified
    Wait Until Element Is Visible  //div[@class='container-fluid page-body-wrapper']/app-sidebar/nav/ul/li[3]  10s
    Click Element  //div[@class='container-fluid page-body-wrapper']/app-sidebar/nav/ul/li[3]
    Wait Until Element Is Visible  //div[@class='text-center m-2 col-sm-6 col-md-6 d-inline']/div/input  10s
    Input Text  //div[@class='text-center m-2 col-sm-6 col-md-6 d-inline']/div/input  ${ValidationCode}
    Wait Until Element Is Visible  //div[@class='text-center m-2 col-sm-6 col-md-6 d-inline']/div/span  10s
    Click Element  //div[@class='text-center m-2 col-sm-6 col-md-6 d-inline']/div/span
Check Verified
    Wait Until Element Is Visible  //div[@class='vh-100 d-block bg-white p-5']/div/div[2]/div[2]/p  10s
    ${Verified_Code}  Get Text  //div[@class='vh-100 d-block bg-white p-5']/div/div[2]/div[2]/p
    IF    '${Verified_Code}' == '${ValidationCode}'
            ${Subject_Name}  Get Text  //div[@class='vh-100 d-block bg-white p-5']/div/div[5]/div[2]/p
            Should Contain  ${Subject_Name}  ${Subject}
            Wait Until Element Is Visible  //div[@class='vh-100 d-block bg-white p-5']/div/div[3]/div[2]/p  10s
            ${Verified_Date}  Get Text  //div[@class='vh-100 d-block bg-white p-5']/div/div[3]/div[2]/p
            Should Contain   ${VerifiedDate}  ${Verified_Date}
            Wait Until Element Is Visible  //div[@class='vh-100 d-block bg-white p-5']/div/div[4]/div[2]/p  10s
            ${Document_No}  Get Text  //div[@class='vh-100 d-block bg-white p-5']/div/div[4]/div[2]/p
            Should Contain   ${Document_No}  ${DocumentNo}
            Wait Until Element Is Visible  //div[@class='vh-100 d-block bg-white p-5']/div/div[6]/div[2]/p  10s
            ${Requester}  Get Text  //div[@class='vh-100 d-block bg-white p-5']/div/div[6]/div[2]/p
            Should Contain   ' ${Requester} '  ${Createdby}
            Wait Until Element Is Visible  //div[@class='vh-100 d-block bg-white p-5']/div/div[8]/div[2]/p  10s
            ${Created_Date}  Get Text  //div[@class='vh-100 d-block bg-white p-5']/div/div[8]/div[2]/p
            Should Contain   ${Created_Date}  ${CreateDate} 
            Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT120-Pass.png
            ${wb}      Load Workbook     ${CURDIR}/${excel}
            Log to Console   ${wb}
            ${ws}      Set Variable  ${wb['Sheet1']}
            Log To Console   ${ws}
            Evaluate   $ws.cell(123,13,'PASS')
            Evaluate   $wb.save('${excel}')
            Close Browser
          ELSE 
            Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT120-Notpass.png
            ${wb}      Load Workbook     ${CURDIR}/${excel}
            Log to Console   ${wb}
            ${ws}      Set Variable  ${wb['Sheet1']}
            Log To Console   ${ws}
            Evaluate   $ws.cell(123,13,'NOT PASS')
            Evaluate   $wb.save('${excel}')
            Close Browser
          END


*** Test Cases ***
Document Details-Pass.
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Filter - Completed
    Choose Tab Verified
    Check Verified
  
    
    