*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot


*** Variables ***
${excel}   SmartFlow.xlsx 
${Document}  ทดสอบรายละเอียดเอกสาร
${NUMBER} =  1
${RequesterShow}  Requester :
${ToShow}  To :
${DetailsRs}
${DetailsTo}
${DetailsLOA}
${Exit}
*** Keywords ***




Input Subject 
    Wait Until Element Is Visible  //div[@class="col-sm-9"]/input  10s
    ${Random Numbers}  generate random string  2  [NUMBERS]
    Set Global Variable  ${Random Numbers}
    Input Text  //div[@class="col-sm-9"]/input  ${Document}(ทดสอบ${Random Numbers})


Get Data Document
    Wait Until Element Is Visible  //div[@class='container-fluid']/div/div[3]/ng-select/div/div/div[2]/span[2]  10s
    ${Requester}  Get Text  //div[@class='container-fluid']/div/div[3]/ng-select/div/div/div[2]/span[2]
    Set Global Variable  ${Requester}
    Log To Console  ${Requester} 
    Wait Until Element Is Visible  //div[@class='container-fluid loa-container']/div/div[1]/div/div[2]/ng-select/div/div/div[2]/span[2]  10s
    ${To}  Get Text  //div[@class='container-fluid loa-container']/div/div[1]/div/div[2]/ng-select/div/div/div[2]/span[2]
    Set Global Variable  ${To}
    Log To Console  ${To}
    Wait Until Element Is Visible  //div[@class='container-fluid loa-container']/div/div[1]/div/div[1]/ng-select/div/div/div[2]/span[2]  10s
    ${LOA}  Get Text  //div[@class='container-fluid loa-container']/div/div[1]/div/div[1]/ng-select/div/div/div[2]/span[2]
    Set Global Variable  ${LOA}
    Log To Console  ${LOA}
    


Click btn Send 
    Click Button  //div[@class='text-center p-3']/button[4]
    Wait Until Page Contains  Your document was created and sent to the approver.  10s
    wait until location is  ${MAINWEB_Requester}  10s

Search Document
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    input Text  //div[@class='col-sm-12 search-box-section']/input  ${Document}(ทดสอบ${Random Numbers})
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot   E:/Smartflow robot/Screenshor_Notpass/RT61-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(64,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
     ${Show}  Get Text  //tbody/tr[1]/td[1]/button/p
                                IF  '${Show}' == '${Document}(ทดสอบ${Random Numbers})'
                                     ${Show}  Get Text  //tbody/tr[1]/td[8]/button/span
                                     Log To Console  ${Show}
                                     IF  '${Show}' == 'Waiting for my Approval'
                                        Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
                                        Click Button  //tbody/tr[1]/td[9]/button
                                        Wait Until Element Is Visible  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[4]/a  10s
                                        Click Element  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[4]/a
                                        ${DetailsRs}  Get Text  //div[@class='tab-pane active']/property-document-details/div/div[2]/p[2]
                                        Log To Console  ${DetailsRs}
                                        Wait Until Element Is Visible  //div[@class='tab-pane active']/property-document-details/div/div[2]/p[3]  10s
                                        ${DetailsTo}  Get Text  //div[@class='tab-pane active']/property-document-details/div/div[2]/p[3]
                                        Log To Console  ${DetailsTo} 
                                        Wait Until Element Is Visible  //div[@class='tab-pane active']/property-document-details/div/div[3]/p  10s
                                        ${DetailsLOA}  Get Text  //div[@class='tab-pane active']/property-document-details/div/div[3]/p
                                        Log To Console  ${DetailsLOA}
                                        ${AllRequesterShow}=  Set Variable   ${RequesterShow} ${Requester} 
                                        Log To Console  ${AllRequesterShow}
                                        Should Contain  ${AllRequesterShow}  ${DetailsRs}
                                        ${AllToShow}=  Set Variable   ${ToShow} ${To} 
                                        Log To Console  ${AllToShow}
                                        Sleep  2
                                        Should Contain  ${AllToShow}  ${DetailsTo}
                                        Sleep  2
                                        Should Contain  ${LOA}   ${DetailsLOA} 
                                        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT61-Pass.png
                                        ${wb}      Load Workbook     ${CURDIR}/${excel}
                                        Log to Console   ${wb}
                                        ${ws}      Set Variable  ${wb['Sheet1']}
                                        Log To Console   ${ws}
                                        Evaluate   $ws.cell(64,13,'PASS')
                                        Evaluate   $wb.save('${excel}')
                                     ELSE
                                     Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT61-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(64,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                     END
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT61-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(64,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                END
                    
    ELSE IF    ${CheckShowData} > ${NUMBER}
                   ${Show}  Get Text  //tbody/tr[${n}]/td[1]/button/p
                                IF  '${Show}' == '${Document}(ทดสอบ${Random Numbers})'
                                     ${Show}  Get Text  //tbody/tr[${n}]/td[8]/button/span
                                     IF  '${Show}' == 'Waiting for my Approval'
                                        Click Button  //tbody/tr[${n}]/td[9]/button
                                        Wait Until Element Is Visible  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[4]/a  10s
                                        Click Element  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[4]/a
                                        ${DetailsRs}  Get Text  //div[@class='tab-pane active']/property-document-details/div/div[2]/p[2]
                                        Log To Console  ${DetailsRs}
                                        Wait Until Element Is Visible  //div[@class='tab-pane active']/property-document-details/div/div[2]/p[3]  10s
                                        ${DetailsTo}  Get Text  //div[@class='tab-pane active']/property-document-details/div/div[2]/p[3]
                                        Log To Console  ${DetailsTo} 
                                        Wait Until Element Is Visible  //div[@class='tab-pane active']/property-document-details/div/div[3]/p  10s
                                        ${DetailsLOA}  Get Text  //div[@class='tab-pane active']/property-document-details/div/div[3]/p
                                        Log To Console  ${DetailsLOA}
                                        
                                        ${AllRequesterShow}=  Set Variable   ${RequesterShow} ${Requester} 
                                        Log To Console  ${AllRequesterShow}
                                        
                                        Should Contain  ${AllRequesterShow}  ${DetailsRs}
                                        ${AllToShow}=  Set Variable   ${ToShow} ${To} 
                                        Log To Console  ${AllToShow}
                                        Sleep  2
                                        Should Contain  ${AllToShow}  ${DetailsTo}
                                        Sleep  2
                                        Should Contain  ${LOA}   ${DetailsLOA} 
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT61-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(64,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    ${Exit}  Set Variable  Exit
                                    Set Global Variable  ${Exit}
                                    Exit For Loop IF  "${Exit}" == "Exit"  
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT61-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(64,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                            END
                        Exit For Loop IF  "${Exit}" == "Exit"  
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT61-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(64,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          
                    END
             Exit For Loop IF  "${Exit}" == "Exit"  
         END
          Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END





*** Test Cases ***
Document Details
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Choose For
    Input Subject 
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Get Data Document
    Click Btn Next
    Click btn Send 
    Sign Out 
    Approver Login (DR) 
    Click Btn Login
    Input OTP And Click OTP
    Search Document
    
    

    
    
  
    