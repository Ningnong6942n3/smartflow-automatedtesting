*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${text}  Pass 
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?
${Employees_NA}
${Pocition_Approver1}  Department Director
${Pocition_Approver2}  Operation Manager
${LOA_NAME}  ทดสอบการส่งรอการอนุมัติLOA 
${LOACode}
${Exit}
*** Keywords ***




Search - LOA 
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval-loa  10s
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    ${CheckFilter}  Run Keyword And Return Status    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[3]  10s 
    Run Keyword If  '${CheckFilter}' != 'True'  Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Click Element  //div[@class='card table-filter']/div/div[3]/ng-select
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[1]  10s
    Click Element  //ng-dropdown-panel/div/div/div[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[2]/input  10s
    Input Text  //div[@class='card table-filter']/div/div[2]/input  ${LOA_NAME}(${Random Numbers})
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot   E:/Smartflow robot/Screenshor_Notpass/RT60-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(63,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Name_LOAS}  Get Text  //tbody/tr[1]/td[2]
                                IF  '${Name_LOAS}' == '${LOA_NAME}(${Random Numbers})'
                                     ${Show}  Get Text  //tbody/tr[1]/td[6]/button/span
                                     IF  '${Show}' == 'Waiting for my Approval'
                                        Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
                                        ${Code_LOA}  Get Text  //tbody/tr[1]/td[1]/button
                                        Set global variable  ${Code_LOA}
                                        Log To Console  ${Code_LOA}
                                        ${Name_LOA}  Get Text  //tbody/tr[1]/td[2]/button
                                        Set global variable  ${Name_LOA} 
                                        Log To Console  ${Name_LOA} 
                                        Wait Until Element Is Visible  //tbody/tr[1]/td[7]/button  10s
                                        Click Button  //tbody/tr[1]/td[7]/button
                                        Wait Until Element Is Visible  //div[@class='modal-body']/div/div/h5  10s
                                        ${Details}  Get Text  //div[@class='modal-body']/div/div/h5
                                        Log To Console  ${Details}
                                        Should Contain  LOA Details  ${Details} 
                                        ${DetailsName}  Get Text  //div[@class='info-wrapper']/div[3]/div[2]/p
                                        Should Contain  ${Name_LOA}  ${DetailsName}
                                        Wait Until Element Is Visible  //div[@class='info-wrapper']/div[2]/div[2]/p  10s
                                        ${DetailsLOACode}  Get Text  //div[@class='info-wrapper']/div[2]/div[2]/p
                                        Should Contain  ${Code_LOA}  ${DetailsLOACode}
                                        Wait Until Element Is Visible  //div[@class='modal-body']/div[2]/button[5]  10s
                                        Click Button  //div[@class='modal-body']/div[2]/button[5]
                                        Wait Until Page Contains  Your LOA was approved  10s
                                        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT88-Pass.png
                                        ${wb}      Load Workbook     ${CURDIR}/${excel}
                                        Log to Console   ${wb}
                                        ${ws}      Set Variable  ${wb['Sheet1']}
                                        Log To Console   ${ws}
                                        Evaluate   $ws.cell(91,13,'PASS')
                                        Evaluate   $wb.save('${excel}')
                                        Close Browser
                                     ELSE
                                     Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT88-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(91,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                     END
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT88-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(91,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                END
                    
    ELSE IF    ${CheckShowData} > ${NUMBER}
                   ${Name_LOAS}  Get Text  //tbody/tr[${n}]/td[2]
                                IF  '${Name_LOAS}' == '${LOA_NAME}(${Random Numbers})'
                                     ${Show}  Get Text  //tbody/tr[1]/td[6]/button/span
                                     IF  '${Show}' == 'Waiting for my Approval'
                                        Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
                                        ${Code_LOA}  Get Text  //tbody/tr[1]/td[1]/button
                                        Set global variable  ${Code_LOA}
                                        Log To Console  ${Code_LOA}
                                        ${Name_LOA}  Get Text  //tbody/tr[1]/td[2]/button
                                        Set global variable  ${Name_LOA} 
                                        Log To Console  ${Name_LOA} 
                                        Wait Until Element Is Visible  //tbody/tr[1]/td[7]/button  10s
                                        Click Button  //tbody/tr[1]/td[7]/button
                                        Wait Until Element Is Visible  //div[@class='modal-body']/div/div/h5  10s
                                        ${Details}  Get Text  //div[@class='modal-body']/div/div/h5
                                        Log To Console  ${Details}
                                        Should Contain  LOA Details  ${Details} 
                                        ${DetailsName}  Get Text  //div[@class='info-wrapper']/div[3]/div[2]/p
                                        Should Contain  ${Name_LOA}  ${DetailsName}
                                        Wait Until Element Is Visible  //div[@class='info-wrapper']/div[2]/div[2]/p  10s
                                        ${DetailsLOACode}  Get Text  //div[@class='info-wrapper']/div[2]/div[2]/p
                                        Should Contain  ${Code_LOA}  ${DetailsLOACode}
                                        Wait Until Element Is Visible  //div[@class='modal-body']/div[2]/button[5]  10s
                                        Click Button  //div[@class='modal-body']/div[2]/button[5]
                                        Wait Until Page Contains  Your LOA was approved  10s
                                        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT88-Pass.png
                                        ${wb}      Load Workbook     ${CURDIR}/${excel}
                                        Log to Console   ${wb}
                                        ${ws}      Set Variable  ${wb['Sheet1']}
                                        Log To Console   ${ws}
                                        Evaluate   $ws.cell(91,13,'PASS')
                                        Evaluate   $wb.save('${excel}')
                                        ${Exit}  Set Variable  Exit
                                        Set Global Variable  ${Exit}
                                        Exit For Loop IF  "${Exit}" == "Exit" 
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT88-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(91,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                            END
                        Exit For Loop IF  "${Exit}" == "Exit"  
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT88-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(91,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          
                    END
             Exit For Loop IF  "${Exit}" == "Exit"  
         END
          Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END





*** Test Cases ***
Designer Approvers - CreateLOA_Saed
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - Create LOA
    Create LOA - Tnput Data (2)
    Choose Manage Approver (2)
    Click btn Save (2)
    Check Create LOA (2)
    Sign Out 
    Approve - Audit_director
    Click Btn Login 
    Input OTP And Click OTP
    Choose Tab LOA (2) 
    Search - LOA 
    
    
   
    
    
    
    
  
    
    
   
    
    

    
    
    