*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot

*** Variables ***
${excel}   SmartFlow.xlsx

*** Keywords ***

Check Error Massage 
    ${ErrorMessage}  Get Text  //div[@class='errors-wrapper']/h6
    IF    '${ErrorMessage}' == '* Username and Password is required!'
              Page Should Contain  * Username and Password is required!
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT4-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(7,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT4-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(7,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END
    
*** Test Cases ***
Test Not input Username and Password
    Open Website SMARTFLOW
    Click Btn Login Only And Not Iuput Data (Requestor)
    Check Error Massage 
    