*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***

${excel}   SmartFlow.xlsx

*** Keywords ***

Input Article
    Input Text  //quill-editor/div/div[@class='ql-editor ql-blank']  BRAINERGY เบรนเนอร์จี เริ่มก่อตั้งเมื่อเดือนธันวาคม 2561 โดยมุ่งหวังเพื่อช่วยให้องค์การของประเทศไทยสามารถใช้ประโยชน์จากเทคโนโลยีดิจิตอลในการทำงานและขยายโอกาสในการทำธุรกิจผ่านระบบดิจิตอลด้วย BRAINERGY เป็นบริษัทในเครือของเบญจจินดาผู้พัฒนาเทคโนโลยีทางด้านการสื่อสารไทยมามากกว่า 50 ปี ทำให้ BRAINERGY สามารถนำศักยภาพเทคโนโลยีดิจิตอลมาตอบโจทย์และเข้าใจการทำงานของธุรกิจไทยได้อย่างเหมาะสม
    Wait Until Page Contains  BRAINERGY เบรนเนอร์จี เริ่มก่อตั้งเมื่อเดือนธันวาคม 2561 โดยมุ่งหวังเพื่อช่วยให้องค์การของประเทศไทยสามารถใช้ประโยชน์จากเทคโนโลยีดิจิตอลในการทำงานและขยายโอกาสในการทำธุรกิจผ่านระบบดิจิตอลด้วย BRAINERGY เป็นบริษัทในเครือของเบญจจินดาผู้พัฒนาเทคโนโลยีทางด้านการสื่อสารไทยมามากกว่า 50 ปี ทำให้ BRAINERGY สามารถนำศักยภาพเทคโนโลยีดิจิตอลมาตอบโจทย์และเข้าใจการทำงานของธุรกิจไทยได้อย่างเหมาะสม
Click btn Preview
    Click Button  //div[@class='text-center p-3']/button[2]
Preview Document
    Sleep  2
    ${Message}  Get Text  //div[@class='modal-body']/h6
    log to console  ${Message}
    IF    '${Message}' == 'Page 1 / 1'
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT17-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(20,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT17-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(20,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END

*** Test Cases ***
CreateDocument-Preview
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Choose For
    Input Subject 
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Click Btn Next
    Input Article
    Click btn Preview
    Preview Document
    
    