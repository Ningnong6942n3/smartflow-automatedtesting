*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?
${Employees_NA}
*** Keywords ***


Click Nav LOA - Edit(Text)
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Click Element  //ul[@class='nav']/li[4]
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[1]/a
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Click Button  //table/tbody/tr[1]/td[3]/div/button
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Location Should Be  ${BULocation01}
    ${checkData}  Get Text  //tbody/tr/td
    Log To Console  ${checkData}
    IF    '${checkData}' == 'No data available in table'
               
                Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[3]  10s
                Click Button  //div[@class='form-btn-wrapper']/button[3]
                Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[1]/div/div  10s
                Click Element  //div[@class='modal-body']/form/div/div[1]/div/div
                
                ${PositionName}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[2]/span
                Log to Console  ${PositionName}
                Set Global Variable  ${PositionName}
                Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[2]  10s
                Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]
                
                Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select  10s
                Click Element  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select
                Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]  10s
                Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]

                Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div[2]/div  10s
                Click Element  //div[@class='modal-body']/form/div/div[2]/div[2]/div
                Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]  10s
                ${EmployeesName_T}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]
                Set Global Variable  ${EmployeesName_T}
                ${EmployeesName}=    Fetch From Left    ${EmployeesName_T}  (
                Set Global Variable  ${EmployeesName}
                Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]  10s
                Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]

                Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[3]/button[1]  10s
                Click Button  //div[@class='modal-body']/form/div/div[3]/button[1]
            
          ELSE
                ${Order_Bu_BF}  Get Text  //tbody/tr[1]/td[1]
                Set Global Variable  ${Order_Bu_BF}
                ${Position_Bu_BF}  Get Text  //tbody/tr[1]/td[2]
                Set Global Variable  ${Position_Bu_BF}
                ${Employees_Bu_BF}  Get Text  //tbody/tr[1]/td[3]
                Set Global Variable  ${Employees_Bu_BF}

                Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[3]  10s
                Click Button  //div[@class='form-btn-wrapper']/button[3]
                Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[1]/div/div  10s
                Click Element  //div[@class='modal-body']/form/div/div[1]/div/div
                
                ${PositionName}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[2]/span
                Log to Console  ${PositionName}
                Set Global Variable  ${PositionName}
                Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[2]  10s
                Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]
                
                Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select  10s
                Click Element  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select
                Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]  10s
                Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]

                Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div[2]/div  10s
                Click Element  //div[@class='modal-body']/form/div/div[2]/div[2]/div
                Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]  10s
                ${EmployeesName_T}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]
                Set Global Variable  ${EmployeesName_T}
                ${EmployeesName}=    Fetch From Left    ${EmployeesName_T}  (
                Set Global Variable  ${EmployeesName}
                Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]  10s
                Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]

                Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[3]/button[1]  10s
                Click Button  //div[@class='modal-body']/form/div/div[3]/button[1]
    END


Check - Add Employees
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]/div/ul/li[1]/a  10s
    Sleep  5
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[1]/a
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //table/tbody/tr[1]/td[3]/div/button
    wait until location is  ${BULocation01}  10s
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${checkData}  Get Text  //tbody/tr/td
    Log To Console  ${checkData}
    IF    '${checkData}' == 'No data available in table'
          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT103-Pass.png
          ${wb}      Load Workbook     ${CURDIR}/${excel}
          Log to Console   ${wb}
          ${ws}      Set Variable  ${wb['Sheet1']}
          Log To Console   ${ws}
          Evaluate   $ws.cell(106,13,'PASS')
          Evaluate   $wb.save('${excel}')
            
    ELSE
          ${Order_Bu_AF}  Get Text  //tbody/tr[1]/td[1]
          Should Contain  ${Order_Bu_AF}  ${Order_Bu_BF}
          Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
          ${Position_Bu_AF}  Get Text  //tbody/tr[1]/td[2]
          Should Contain  ${Position_Bu_AF}  ${Position_Bu_BF}
          Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
          ${Employees_Bu_AF}  Get Text  //tbody/tr[1]/td[3]
          Should Contain  ${Employees_Bu_AF}  ${Employees_Bu_BF}
          Wait Until Element Is Visible  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
          ${countSh} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
          Set Global Variable  ${countSh}
          FOR    ${x}    IN RANGE    ${countSh}
            ${n}  Evaluate  ${x}+1
            Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${n}]
            ${count} =	Get Element Count  //div[@class='col-sm-12']/table/tbody/tr
              FOR    ${z}    IN RANGE    ${count}
                ${zn}  Evaluate  ${z}+1
                ${Data}  Get Text  //tbody/tr[${zn}]/td[2]
                Log To Console  ${Data}
                IF  '${Data}' == '${PositionName}'
                  ${Employees_A}  Get Text  //tbody/tr[${zn}]/td[3]
                  ${Employees_NA} =    Fetch From Left  ${Employees_A}    (
                  Set Global Variable  ${Employees_NA}
                  Log to Console  ${Employees_NA}    
                  Log to Console  ${EmployeesName}
                              IF  '${Employees_NA}' == '${EmployeesName}'
                                  Log To Console  ${PositionName}  ${Employees_NA}
                                  Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT103-Notpass.png
                                  ${wb}      Load Workbook     ${CURDIR}/${excel}
                                  Log to Console   ${wb}
                                  ${ws}      Set Variable  ${wb['Sheet1']}
                                  Log To Console   ${ws}
                                  Evaluate   $ws.cell(106,13,'NOT PASS')
                                  Evaluate   $wb.save('${excel}')
                                  ${Exit}  Set Variable  Exit
                                  Set Global Variable  ${Exit}
                                  EXIT FOR LOOP IF  '${Employees_NA}' == '${EmployeesName}'
                              ELSE  
                                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT103-Pass.png
                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                Log to Console   ${wb}
                                ${ws}      Set Variable  ${wb['Sheet1']}
                                Log To Console   ${ws}
                                Evaluate   $ws.cell(106,13,'PASS')
                                Evaluate   $wb.save('${excel}')
                              END
                ELSE 
                  Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT103-Pass.png
                  ${wb}      Load Workbook     ${CURDIR}/${excel}
                  Log to Console   ${wb}
                  ${ws}      Set Variable  ${wb['Sheet1']}
                  Log To Console   ${ws}
                  Evaluate   $ws.cell(106,13,'PASS')
                  Evaluate   $wb.save('${excel}')
                  EXIT FOR LOOP IF  '${Employees_NA}' == '${EmployeesName}'
                END
              END 
              EXIT FOR LOOP IF  '${Employees_NA}' == '${EmployeesName}'
          END 
          Close Browser    
    END
    
    



*** Test Cases ***
LOA Designer Users - Edit_AddEmployees_don'tSave
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - Edit(Text)
    Check - Add Employees
    
    
  
    
    
   
    
    

    
    
    