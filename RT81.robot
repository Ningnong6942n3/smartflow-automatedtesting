*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?
${Employees_NA}
${LOANameError}  Please insert LOA Name
${BusinessUnitError}  Please select BU
${LOADescriptionError}  Please insert LOA Description
${ManageApproverError}  Please select Approver
*** Keywords ***




    
Create LOA - Tnput Data
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[1]/input  10s
    Input Text  //div[@class='form-content-wrapper']/div/div[1]/input  LOAName01
    Wait Until Element Is Visible  //div[@class='form-action-wrapper']/button[2]  10s
    Click Button  //div[@class='form-action-wrapper']/button[2]

Check Error Messages 
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[2]/div/div  10s
    ${BusinessUnit_Error}  Get Text  //div[@class='form-content-wrapper']/div/div[2]/div/div
    Should Contain  ${BusinessUnit_Error}  ${BusinessUnitError}
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[3]/div/div  10s
    ${LOADescription_Error}  Get Text  //div[@class='form-content-wrapper']/div/div[3]/div/div
    Should Contain  ${LOADescription_Error}  ${LOADescriptionError}
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/div/div/div/div  10s
    ${ManageApprover_Error}  Get Text  //div[@class='form-btn-wrapper']/div/div/div/div
    Should Contain  ${ManageApprover_Error}  ${ManageApproverError}
                              IF   '${BusinessUnit_Error}' == '${BusinessUnitError}'
                                
                                            IF   '${LOADescription_Error}' == '${LOADescriptionError}'
                                                
                                                        IF   '${ManageApprover_Error}' == '${ManageApproverError}'
                                                               
                                                                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT81-Pass.png
                                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                                Log to Console   ${wb}
                                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                                Log To Console   ${ws}
                                                                Evaluate   $ws.cell(84,13,'PASS')
                                                                Evaluate   $wb.save('${excel}')
                                                                Close Browser
                                                        ELSE
                                                                Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT81-Notpass.png
                                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                                Log to Console   ${wb}
                                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                                Log To Console   ${ws}
                                                                Evaluate   $ws.cell(84,13,'NOT PASS')
                                                                Evaluate   $wb.save('${excel}')
                                                                Close Browser
                                                        END
                                            ELSE
                                                Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT81-Notpass.png
                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                Log to Console   ${wb}
                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                Log To Console   ${ws}
                                                Evaluate   $ws.cell(84,13,'NOT PASS')
                                                Evaluate   $wb.save('${excel}')
                                                Close Browser
                                            END
                              ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT81-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(84,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                    Close Browser
                              END
    


*** Test Cases ***
Designer Approvers - CreateLOA_Error02
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - Create LOA
    Create LOA - Tnput Data
    Check Error Messages 
   

    
    
  
    
    
   
    
    

    
    
    