*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${text}  Pass 
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?

*** Keywords ***


    
    
Edit - Cancle
    wait until location is  ${BULocation01}  10s
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[1]  10s
    Sleep  5
    Click Button  //div[@class='form-btn-wrapper']/button[1]
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div[1]/p  10s
    ${PopUpConfirm}  Get Text  //div[@class='modal-body']/div/div[1]/p
    Should Contain  ${PopUpConfirm}  ${PopUpCancle}
    Click Button  //div[@class='modal-body']/div/div[2]/button[1]
    wait until location is  ${BULocation01}  10s
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[1]  10s
    Click Button  //div[@class='form-btn-wrapper']/button[1]
    ${PopUpConfirm}  Get Text  //div[@class='modal-body']/div/div[1]/p
    Should Contain  ${PopUpConfirm}  ${PopUpCancle}
    Click Button  //div[@class='modal-body']/div/div[2]/button[2]
    ${Nowurl}=   Get Location
    wait until location is  ${Nowurl}  10s
    IF  '${Nowurl}' == '${MainLOA_Designer_Users}'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT105-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(108,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser
    ELSE  
        Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT105-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(108,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser
    END
  



*** Test Cases ***
LOA Designer Users - Edit_Cancle
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP 
    Click Nav LOA - Edit (Tab)
    Edit - Cancle
    
    
    

    
    
    