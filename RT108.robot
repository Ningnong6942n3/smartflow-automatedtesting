*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${text}  Pass 
${NUMBER}  1
${ErrorPosition}  Please select Position
${ErrorEmployees}  Please select Employee

*** Keywords ***





Edit - Add Employees
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[3]  10s
    Click Button  //div[@class='form-btn-wrapper']/button[3]
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Element  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='modal-body']/form/div/div[3]/button[1]
    

Error Massage 
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[1]/div/div/div/div  10s
    ${ErrorMassage1}  Get Text  //div[@class='modal-body']/form/div/div[1]/div/div/div/div
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div[2]/div/div/div  10s
    ${ErrorMassage2}  Get Text  //div[@class='modal-body']/form/div/div[2]/div[2]/div/div/div
    IF  '${ErrorMassage1}' == '${ErrorPosition}'
            IF  '${ErrorMassage2}' == '${ErrorEmployees}'
                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT108-Pass.png
                ${wb}      Load Workbook     ${CURDIR}/${excel}
                Log to Console   ${wb}
                ${ws}      Set Variable  ${wb['Sheet1']}
                Log To Console   ${ws}
                Evaluate   $ws.cell(111,13,'PASS')
                Evaluate   $wb.save('${excel}')
                Close Browser
            ELSE  
                Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT108-Notpass.png
                ${wb}      Load Workbook     ${CURDIR}/${excel}
                Log to Console   ${wb}
                ${ws}      Set Variable  ${wb['Sheet1']}
                Log To Console   ${ws}
                Evaluate   $ws.cell(111,13,'NOT PASS')
                Evaluate   $wb.save('${excel}')
                Close Browser
            END
    ELSE  
        Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT108-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(111,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser
    END


*** Test Cases ***
EditAddEmployees_Error3
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP 
    Click Nav LOA - Edit (Tab)
    Edit - Add Employees
    Error Massage 
   
    
    

    
    
    