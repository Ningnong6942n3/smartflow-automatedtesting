*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot


*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${WEBEdit}  https://shl-dev.brainergy.digital/work-space/profile
*** Keywords ***


Click Tab Profile
    Wait Until Element Is Visible  //li[@class='nav-item nav-profile']/a  10s
    Click Element  //li[@class='nav-item nav-profile']/a

Click Btn Signature
    Wait Until Element Is Visible  //div[@class='content-wrapper']/div/app-profile/div/div/div[9]/div/div/div/button  10s
    Click Element  //div[@class='content-wrapper']/div/app-profile/div/div/div[9]/div/div/div/button
    Page Should Contain Button  SIGN NOW
    ${SIGN_NOW}  Get Text  //div[@class='modal-body']/div/button[1]
    Should Contain  ${SIGN_NOW}  SIGN NOW
    Page Should Contain Button  IMPORT
    Wait Until Element Is Visible  //div[@class='modal-body']/div/button[2]  10s
    ${IMPORT}  Get Text  //div[@class='modal-body']/div/button[2]
    Should Contain  ${IMPORT}  IMPORT
    Page Should Contain Button  CANCEL
    Wait Until Element Is Visible  //div[@class='modal-body']/div/button[3]  10s
    ${CANCEL}  Get Text  //div[@class='modal-body']/div/button[3]
    Should Contain  ${CANCEL}  CANCEL
    

Click Btn Clear
    Wait Until Element Is Visible  //div[@class='modal-body']/div/button[3]  10s
    Click Button  //div[@class='modal-body']/div/button[3]
    ${Nowurl}=   Get Location
    Sleep  5
    IF   '${Nowurl}' == '${WEBEdit}'
          Capture Page Screenshot  E:/TEstAll/Screenshot_Pass/RT121-Pass.png
          ${wb}      Load Workbook     ${CURDIR}/${excel}
          Log to Console   ${wb}
          ${ws}      Set Variable  ${wb['Sheet1']}
          Log To Console   ${ws}
          Evaluate   $ws.cell(124,13,'PASS')
          Evaluate   $wb.save('${excel}')
          Close Browser 
    ELSE
          Capture Page Screenshot  E:/TEstAll/Screenshor_Notpass/RT121-Notpass.png
          ${wb}      Load Workbook     ${CURDIR}/${excel}
          Log to Console   ${wb}
          ${ws}      Set Variable  ${wb['Sheet1']}
          Log To Console   ${ws}
          Evaluate   $ws.cell(124,13,'NOT PASS')
          Evaluate   $wb.save('${excel}')
          Close Browser 
    END
    
*** Test Cases ***
Document Details-Pass.
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Tab Profile
    Click Btn Signature
    Click Btn Clear
    
    
    
    