*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot


*** Variables ***
${excel}   SmartFlow.xlsx
${text}  Pass 
${Data}
${Exit}
${NUMBER}  1
*** Keywords ***


Search Document 
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval  10s
    Click Element  //div[@class]/div[@class='h-100']/app-waiting-for-my-approval/ul/li[2]
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/my-approval  10s
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    ${Data2}  Get Text   //tbody/tr[1]/td/button/p
    input Text  //div[@class='col-sm-12 search-box-section']/input  ${Data2}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT66-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(69,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Sleep  2 
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Data}  Get Text  //tbody/tr[1]/td[1]/button/p 
       Log To Console   ${Data} Test
       Log To Console   ${Data2} Test
                    IF   '${Data2}' == '${Data}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT66-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(69,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Log To Console   f1
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT66-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(69,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                    ${Data}  Get Text  //tbody/tr[${n}]/td[1]/button/p 
                    Log To Console   ${Data}
                    Log To Console   ${Data2}
                    IF    '${Data2}' == '${Data}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT66-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(69,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Log To Console   f2
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT66-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(69,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
         Exit For Loop IF  "${Exit}" == "Exit"  
         END
         Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END


*** Test Cases ***
MY APPROVAL - Search
    Open Website SMARTFLOW
    Approver Login (DR)
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Search Document 
    
   
    
   
    