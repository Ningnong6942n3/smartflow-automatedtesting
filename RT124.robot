*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     AutoitLibrary
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx

*** Keywords ***



Filter Documents 
    Wait Until Element Is Visible  //tbody/tr/td  40s
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[3]  10s
    Click Element  //div[@class='card table-filter']/div/div[3]
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[5]  10s
    Click Element  //ng-dropdown-panel/div/div/div[5]
    
Click Btn Download 
    Wait Until Element Is Visible  //tbody/tr/td  20s
    Click Element  //tbody/tr[1]/td[10]
    Wait Until Element Is Visible  //div[@class='left-container']/div/button[2]  10s
    Click Button  //div[@class='left-container']/div/button[2]
    ${Nowurl}=   Get Location
    Set global variable  ${Nowurl}

Check Download
    Sleep  5
    ${handle} =	Switch Window	NEW
    ${NExturl}=   Get Location
    Log to Console  ${NExturl}
    IF   '${Nowurl}' != '${NExturl}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT124-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(127,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
     ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT124-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(127,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
     END

*** Test Cases ***
UserManuals
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Filter Documents 
    Click Btn Download 
    Check Download
    

    
    
    