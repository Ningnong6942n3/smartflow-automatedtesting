*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot


*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${InputText}  Return to Requester
${Exit}
*** Keywords ***


Click Btn Preview - Return
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${NameDocument}  Get Text  //tbody/tr[4]/td[1]/button/p
    Set Global Variable  ${NameDocument}
    ${StatusData}  Get Text  //tbody/tr[4]/td[6]/div/button
    ${StatusDataNow}  Fetch From Right  ${StatusData}  /
    Click Button  //tbody/tr[4]/td[9]/button
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/details/BNG2021%252F${StatusDataNow}  10s
    Wait Until Element Is Visible  //div[@class='text-center']/button[2]  10s
    Click Button  //div[@class='text-center']/button[2]
    ${Return}  Get Text  //div[@class='modal-body pt-0']/div/div/h3
    Should Contain  Return  ${Return} 
    Input Text  //div[@class='modal-body pt-0']/div/div[2]/textarea  Return to Requester
    Click Button  //div[@class='modal-footer border-top-0 d-block pt-0']/div/button
    Wait Until Page Contains  Your document was return.  10s
    Wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval  10s

Check Status Return
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div  10s
    Click Element  //div[@class='card table-filter']/div/div[3]
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div  10s
    Click Element  //ng-dropdown-panel/div/div/div[3]
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
        Click Button  //tbody/tr[1]/td[10]/button
        Wait Until Element Is Visible  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/label/label/span  10s
        ${Status}  Get Text  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/label/label/span
        Should Contain     Returned     ${Status}
        Click Element  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/label/label/button
        ${MailReturn}  Get Text  //div[@class='modal-body']/div/h6
        Should Contain    Return Reason :    ${MailReturn}
        ${TextReturn}  Get Text  //div[@class='modal-body']/div/p
        Should Contain    ${InputText}    ${TextReturn}
    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT58-Pass.png
    ${wb}      Load Workbook     ${CURDIR}/${excel}
    Log to Console   ${wb}
    ${ws}      Set Variable  ${wb['Sheet1']}
    Log To Console   ${ws}
    Evaluate   $ws.cell(61,13,'PASS')
    Evaluate   $wb.save('${excel}')
    Close Browser
    


*** Test Cases ***
WAITING FOR MY APPROVAL Preview - Return
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Check Status Return
    
   
   
    

    
    
    