*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx 
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?
${Employees_NA}
*** Keywords ***







Click Nav LOA - Edit(Save)
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Click Element  //ul[@class='nav']/li[4]
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[1]/a
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Click Button  //table/tbody/tr[1]/td[3]/div/button
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Location Should Be  ${BULocation01}
    ${checkData}  Get Text  //tbody/tr/td
    Log To Console  ${checkData}

    IF    '${checkData}' == 'No data available in table'
            
            Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[3]  10s
            Click Button  //div[@class='form-btn-wrapper']/button[3]
            Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[1]/div/div  10s
            Click Element  //div[@class='modal-body']/form/div/div[1]/div/div

           
            ${Random Numbers}  Evaluate    random.sample(range(1,10), 1)
            Log To Console  ${numbers}
            ${PositionName}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}/span
            Set Global Variable  ${PositionName}
            Log To Console  ${PositionName}
            Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}  10s
            Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}
            
            Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select  10s
            Click Element  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select
            Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]  10s
            Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]
            
            ${Random Numbers}  generate random string  1  [NUMBERS]
            Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div[2]/div  10s
            Click Element  //div[@class='modal-body']/form/div/div[2]/div[2]/div
            Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}  10s
            ${EmployeesName_T}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}
            Set Global Variable  ${EmployeesName_T}
            ${EmployeesName}=    Fetch From Left    ${EmployeesName_T}  (
            Set Global Variable  ${EmployeesName}
            Log To Console  ${EmployeesName}
            Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}  10s
            Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}
            
            Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[3]/button[1]  10s
            Click Button  //div[@class='modal-body']/form/div/div[3]/button[1]
          ELSE
            Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[3]  10s
            Click Button  //div[@class='form-btn-wrapper']/button[3]
            Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[1]/div/div  10s
            Click Element  //div[@class='modal-body']/form/div/div[1]/div/div

           
            ${Random Numbers}  Evaluate    random.sample(range(1,10), 1)
            ${PositionName}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}/span
            Set Global Variable  ${PositionName}
             Log To Console  ${PositionName}
            Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}  10s
            
            Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}
            Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select  10s
            Click Element  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select
            Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]  10s
            Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]
            
            ${Random Numbers}  Evaluate    random.sample(range(1,10), 1)
            Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div[2]/div  10s
            Click Element  //div[@class='modal-body']/form/div/div[2]/div[2]/div
            Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}  10s
            ${EmployeesName_T}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}
            Set Global Variable  ${EmployeesName_T}
            ${EmployeesName}=    Fetch From Left    ${EmployeesName_T}  (
            Set Global Variable  ${EmployeesName}
             Log To Console  ${EmployeesName}
            Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}  10s
            Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div${Random Numbers}
            
            Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[3]/button[1]  10s
            Click Button  //div[@class='modal-body']/form/div/div[3]/button[1]
    END
    

Click Btn Save
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[2]  10s
    Click Button  //div[@class='form-btn-wrapper']/button[2]
    Wait Until Page Contains  The Employee was saved successfully  10s
    
Check - Add Employees After Save
    wait until location is  https://shl-dev.brainergy.digital/work-space/loa/designer-users/edit-users/32  10s
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]  10s
    Sleep  5
    Click Element  //ul[@class='nav']/li[4]
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]/div/ul/li[1]/a  10s
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[1]/a
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //table/tbody/tr[1]/td[3]/div/button
    Wait Until Element Is Visible  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a  10s
    ${countSh} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    Set Global Variable  ${countSh}
    FOR    ${x}    IN RANGE    ${countSh}
      ${n}  Evaluate  ${x}+1
      Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${n}]
      ${count} =	Get Element Count  //div[@class='col-sm-12']/table/tbody/tr
      Log to Console  ${count}
         FOR    ${z}    IN RANGE    ${count}
           ${zn}  Evaluate  ${z}+1
           ${Data}  Get Text  //tbody/tr[${zn}]/td[2]
           Log To Console  ${Data}
            Log To Console  ${PositionName}
           IF  '${Data}' == '${PositionName}'
            ${Employees_A}  Get Text  //tbody/tr[${zn}]/td[3]
            ${Employees_NA} =    Fetch From Left  ${Employees_A}    (
            Set Global Variable  ${Employees_NA}
            Log To Console  ${EmployeesName} Test
            Log To Console  ${Employees_NA} Test
              IF  '${Employees_NA}' == '${EmployeesName}'
                Log To Console  ${PositionName}  ${Employees_NA}
                Click Element   //button[@class='btn btn-delete-employee']
                Wait Until Element Is Visible  //h3[contains(text(),'Delete')]  10s
                Wait Until Element Is Visible  //button[contains(text(),'Delete')]  10s
                Click Element   //button[contains(text(),'Delete')]
                Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[2]  10s
                Click Button  //div[@class='form-btn-wrapper']/button[2]
                Wait Until Page Contains  The Employee was saved successfully  10s
                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT104-Pass.png
                ${wb}      Load Workbook     ${CURDIR}/${excel}
                Log to Console   ${wb}
                ${ws}      Set Variable  ${wb['Sheet1']}
                Log To Console   ${ws}
                Evaluate   $ws.cell(107,13,'PASS')
                Evaluate   $wb.save('${excel}')
                EXIT FOR LOOP IF  '${Employees_NA}' == '${EmployeesName}'
              ELSE  
                
                ${wb}      Load Workbook     ${CURDIR}/${excel}
                Log to Console   ${wb}
                ${ws}      Set Variable  ${wb['Sheet1']}
                Log To Console   ${ws}
                Evaluate   $ws.cell(107,13,'NOT PASS')
                Evaluate   $wb.save('${excel}')
                
              END
           ELSE  
            ${wb}      Load Workbook     ${CURDIR}/${excel}
            Log to Console   ${wb}
            ${ws}      Set Variable  ${wb['Sheet1']}
            Log To Console   ${ws}
            Evaluate   $ws.cell(107,13,'NOT PASS')
            Evaluate   $wb.save('${excel}')
            EXIT FOR LOOP IF  '${Employees_NA}' == '${EmployeesName}'
           END
         END 
         EXIT FOR LOOP IF  '${Employees_NA}' == '${EmployeesName}'
    END 
    Close Browser
    





*** Test Cases ***
LOA Designer Users - Edit_AddEmployees_Save
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - Edit(Save)
    Click Btn Save
    Check - Add Employees After Save
    
 
    
    
    
    

    
    
    